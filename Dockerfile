# Use a imagem base Ubuntu
FROM ubuntu:xenial

# Atualize a lista de pacotes e instale as ferramentas de troubleshooting
RUN apt-get update -y \
    && apt-get install -y \
      vim \
      curl \
      traceroute \
      mtr \
      tcpdump \
      net-tools \
      iputils-ping \
      telnet \
      netcat \
      dnsutils \
      postgresql-client

# Limpa os arquivos temporários do APT
RUN apt-get clean \
    && rm -rf /var/lib/apt/lists/*

# Defina o comando padrão ao iniciar o container como o shell bash
CMD [ "/bin/bash" ]
