# Semantic Versioning Changelog

## 1.0.0 (2023-01-17)


### :sparkles: News

*  modificando yml ([f302300](https://gitlab.com/nuageit-community/network-tools/commit/f30230008fd21c6c608b1e27e6a8ee46ad5b94f2))
* add ci ([95787cb](https://gitlab.com/nuageit-community/network-tools/commit/95787cb5fcdcd3542daeca3a34b0f04ddbf7c6bd))
* add config files ([0326390](https://gitlab.com/nuageit-community/network-tools/commit/0326390aa00217d469a174d8c6b7994b5c51a161))
* add config files ([7331585](https://gitlab.com/nuageit-community/network-tools/commit/73315850b51cf21ac85bad37ecce56880693dd96))
* add config files ([e482190](https://gitlab.com/nuageit-community/network-tools/commit/e4821905d0cc2b290332e1a55c3756cadc88232e))


### :repeat: CI

* adicionando novo template pipeline ([90ad64e](https://gitlab.com/nuageit-community/network-tools/commit/90ad64ebdaeccde80942bf4c76ae56edcc669992))
* correcao include ([ed4b691](https://gitlab.com/nuageit-community/network-tools/commit/ed4b691fe3a933f92e030d17191560e96ed11d22))
* deletando variaveis ([ecc521b](https://gitlab.com/nuageit-community/network-tools/commit/ecc521bdbddc0eec692a7c9835651012682d944f))


### :bug: Fixes

* alterando namespace ([f840f4e](https://gitlab.com/nuageit-community/network-tools/commit/f840f4e702ca898842eecf163a2359d186763432))
* aplicando deploy ([7870678](https://gitlab.com/nuageit-community/network-tools/commit/78706781f2e2f1e52affe90883933e77ebaa4969))
* configurando namespace ([2ff6761](https://gitlab.com/nuageit-community/network-tools/commit/2ff676170f8c137bc4b8464f9eb98538649234c8))
* dockerfile style ([5ac46cb](https://gitlab.com/nuageit-community/network-tools/commit/5ac46cbbdb02a39f9cf88b5831639c90a5db9dfc))
* package.json file ([c39aed5](https://gitlab.com/nuageit-community/network-tools/commit/c39aed5456a9b0afc4942aadfc46a2b4dabf1cca))
